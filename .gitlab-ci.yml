stages:
  - pre-build
  - build
  - package
  - security
  - report

variables:
  IMAGE_PIPELINE: "python:3"
  IMAGE_NAME: "$CI_REGISTRY_IMAGE/$CI_COMMIT_SHA"

# Templates stage
.pre-build: &pre-build-job
  image: $IMAGE_PIPELINE
  stage: pre-build
  before_script:
    - mkdir .artifacts
  allow_failure: true

.build: &build-job
  image: $IMAGE_PIPELINE
  stage: build
  before_script:
    - pip install -U pip -r src/requirements.txt

.security: &security-job
  image: $IMAGE_PIPELINE
  stage: security
  before_script:
    - mkdir .artifacts
  allow_failure: true

# Fin Templates

python-linting:
  <<: *pre-build-job
  script:
    - pip install -U pip pylint
    - pylint src -r n | tee .artifacts/pylint-report.txt
  artifacts:
    paths:
      - .artifacts/pylint-report.txt
    when: always
  except:
    variables:
      - $PYTHON_LINTING_DISABLED

docker-linting:
  <<: *pre-build-job
  image: hadolint/hadolint
  script:
    - hadolint --trusted-registry docker.io --trusted-registry $CI_REGISTRY Dockerfile | tee .artifacts/hadolint-report.txt
  except:
    variables:
      - $DOCKERLINT_LINTING_DISABLED

python-test:
  <<: *build-job
  script:
    - pip install -U pip pytest
    - pytest

covering-test:
  <<: *build-job
  script:
    - pip install -U pip pytest pytest-cov
    - pytest --cov-report term-missing --cov=src test
    - coverage xml -i
  artifacts:
    paths:
      - coverage.xml

docker-package:
  image: docker:19.03.8
  stage: package
  services:
    - docker:19.03.8-dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build --pull --tag $IMAGE_NAME .
    - docker push $IMAGE_NAME

bandit-security:
  <<: *security-job
  script:
    - pip install -U pip bandit
    - bandit --verbose --ignore-nosec --recursive -o .artifacts/bandit_report.json -f json src
  except:
    variables:
      - $BANDIT_DISABLED
  artifacts:
    paths:
      - .artifacts/bandit_report.json
    when: always

safety-security:
  <<: *security-job
  script:
    - pip install -U pip safety
    - safety check -r src/requirements.txt | tee .artifacts/safety_report.txt
  except:
    variables:
      - $SAFETY_DISABLED

trivy-security:
  <<: *security-job
  image:
    name: aquasec/trivy:latest
    entrypoint: [""]
  script:
    - export TRIVY_AUTH_URL=$CI_REGISTRY
    - export TRIVY_USERNAME=$CI_REGISTRY_USER
    - export TRIVY_PASSWORD=$CI_REGISTRY_PASSWORD
    - trivy --exit-code 0 --cache-dir .trivycache/ --no-progress --format table "$IMAGE_NAME"
    - trivy --exit-code 0 --cache-dir .trivycache/ --no-progress --format json -o .artifacts/trivy_report.json "$IMAGE_NAME"
    - trivy --exit-code 1 --cache-dir .trivycache/ --severity HIGH,CRITICAL --no-progress "$IMAGE_NAME"
  except:
    variables:
      - $TRIVY_DISABLED

sonar-report:
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  variables:
    SONAR_HOST_URL: $SONAR_HOST_URL
    SONAR_TOKEN: $SONAR_TOKEN
  stage: report
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - sonar-scanner
      -Dsonar.qualitygate.wait=true
      -Dsonar.projectKey=$CI_PROJECT_PATH_SLUG
      -Dsonar.projectName=$CI_PROJECT_TITLE
      -Dsonar.projectVersion=$CI_COMMIT_SHORT_SHA
      -Dsonar.python.pylint.reportPath=.artifacts/pylint-report.txt
      -Dsonar.python.pylint_config=.pylintrc
      -Dsonar.python.coverage.reportPaths=coverage.xml
      -Dsonar.python.bandit.reportPaths=.artifacts/bandit_report.json
      -Dsonar.sourceEncoding=UTF-8
      -Dsonar.language=py
      -Dsonar.exclusions=**/*.pyc,**/*.ini
      -Dsonar.test.exclusions=test/
